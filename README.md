**Phone number country lookup by call code**

*Production host:*

Demo hosted at: http://neotech-phone.paas.leviracloud.eu/

*Requirements, implementation, known issues:*

Call codes data loaded from: https://en.wikipedia.org/wiki/List_of_country_calling_codes
on production service will refresh data each time starts

restcountries.eu web service used only to get country names by codes

There is unresolved issue with North America countries.
All this countries starts with code +1 but have own call codes table. (USA, Canada..)
To implement detecting need use another WIKI pages(or other sources)

*REST web service entry points:*

check phone number:

GET 
ws/check-country-by-phone-number/{phoneNumber}

get all countries with related call codes

GET 
ws/country-call-codes

*Front-end:*

see index page of production or localhost
general requirements and also can see all currently loaded countries with codes

*Tests coverage:*

acceptance tests for both entry points,
integration tests for general required parts (services, initialization)

tests use in-memory DB

*Database:*

There is 2 options for localhost: 
use of local DB or remote DB. (by default). 
(see application.properties params)

Call codes will be loaded by service each time application starts
(if necessary can be disabled in properties file) 