package lv.neotech.domain.repository;

import lv.neotech.domain.model.CountryDialing;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
public interface CountryDialingRepository extends CrudRepository<CountryDialing, String> {
	@Query("SELECT c.countryCode FROM CountryDialing c JOIN c.callCodes cc WHERE cc = :callCode")
	List<String> findAllByCallCodesContains(@Param("callCode") String callCode);

	@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
	@Query("SELECT c.countryCode FROM CountryDialing c JOIN c.callCodes cc WHERE cc LIKE :callCode||'%'")
	List<String> findAllByCallCodesStartingWith(@Param("callCode") String callCode);
}
