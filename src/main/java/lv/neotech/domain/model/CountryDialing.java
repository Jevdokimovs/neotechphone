package lv.neotech.domain.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "country_dialing")
public class CountryDialing {
	@PrePersist
	protected void onCreate() {
		created = LocalDateTime.now();
	}

	@Id
	@Column(name = "country_code", nullable = false, unique = true, length = 5)
	private String countryCode;

	@Column(
			name = "created",
			nullable = false,
			updatable = false
	)
	private LocalDateTime created;

	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(
			name = "call_code", 
			joinColumns = @JoinColumn(name = "country_code", referencedColumnName = "country_code")
	)
	@Column(name="code", nullable = false)
	private Set<String> callCodes = new HashSet<>();

	public CountryDialing(String countryCode, Set<String> callCodes) {
		this.countryCode = countryCode;
		this.callCodes = callCodes;
	}

	@SuppressWarnings("unused")
	public CountryDialing() {
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Set<String> getCallCodes() {
		return callCodes;
	}

	public void setCallCodes(Set<String> callCodes) {
		this.callCodes = callCodes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CountryDialing)) return false;
		CountryDialing that = (CountryDialing) o;
		return Objects.equal(countryCode, that.countryCode);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(countryCode);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("countryCode", countryCode)
				.add("callCodes", callCodes)
				.toString();
	}
}