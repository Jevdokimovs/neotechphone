package lv.neotech.domain.dto;

import java.util.Set;

public class CountryLookupResultDTOBuilder {
	private Set<String> errors;
	private Set<CountryDTO> countries;
	private boolean success;

	public CountryLookupResultDTOBuilder errors(Set<String> errors) {
		this.errors = errors;
		return this;
	}

	public CountryLookupResultDTOBuilder countries(Set<CountryDTO> countries) {
		this.countries = countries;
		return this;
	}

	public CountryLookupResultDTOBuilder success(boolean success) {
		this.success = success;
		return this;
	}

	public CountryLookupResultDTO build() {
		return new CountryLookupResultDTO(errors, countries, success);
	}
}