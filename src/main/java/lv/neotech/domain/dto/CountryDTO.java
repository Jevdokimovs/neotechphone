package lv.neotech.domain.dto;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class CountryDTO {
	private String code;
	private String name;

	@SuppressWarnings("unused")
	public CountryDTO() {
	}

	public CountryDTO(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CountryDTO)) return false;
		CountryDTO that = (CountryDTO) o;
		return Objects.equal(code, that.code);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(code);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("code", code)
				.add("name", name)
				.toString();
	}
}
