package lv.neotech.domain.dto;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.Set;

public class CountryListItemDTO {
	private String countryCode;
	private Set<String> callCodes;

	public CountryListItemDTO() {
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Set<String> getCallCodes() {
		return callCodes;
	}

	public void setCallCodes(Set<String> callCodes) {
		this.callCodes = callCodes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CountryListItemDTO)) return false;
		CountryListItemDTO that = (CountryListItemDTO) o;
		return Objects.equal(countryCode, that.countryCode);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(countryCode);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("countryCode", countryCode)
				.add("callCodes", callCodes)
				.toString();
	}
}
