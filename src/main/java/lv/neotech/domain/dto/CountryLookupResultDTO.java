package lv.neotech.domain.dto;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public class CountryLookupResultDTO {
	private Set<String> errors = newHashSet();
	private Set<CountryDTO> countries = newHashSet();
	private boolean success = true;

	public CountryLookupResultDTO() {
	}

	CountryLookupResultDTO(
			Set<String> errors,
			Set<CountryDTO> countries,
			boolean success
	) {
		this.errors = errors;
		this.countries = countries;
		this.success = success;
	}

	public Set<String> getErrors() {
		return errors;
	}

	public void setErrors(Set<String> errors) {
		this.errors = errors;
	}

	public Set<CountryDTO> getCountries() {
		return countries;
	}

	public void setCountries(Set<CountryDTO> countries) {
		this.countries = countries;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void addCountry(CountryDTO countryDTO) {
		countries.add(countryDTO);
	}

	public void addError(String error) {
		errors.add(error);
		success = false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CountryLookupResultDTO)) return false;
		CountryLookupResultDTO that = (CountryLookupResultDTO) o;
		return success == that.success &&
				Objects.equal(errors, that.errors) &&
				Objects.equal(countries, that.countries);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(errors, countries, success);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("errors", errors)
				.add("countries", countries)
				.add("success", success)
				.toString();
	}
}
