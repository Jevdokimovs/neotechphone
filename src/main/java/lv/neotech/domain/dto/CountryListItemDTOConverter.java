package lv.neotech.domain.dto;

import lv.neotech.domain.model.CountryDialing;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CountryListItemDTOConverter {
	public List<CountryListItemDTO> convertList(List<CountryDialing> list) {
		return list.stream().map(this::convert).collect(Collectors.toList());
	}

	private CountryListItemDTO convert(CountryDialing entity) {
		CountryListItemDTO dto = new CountryListItemDTO();
		BeanUtils.copyProperties(
				entity,
				dto,
				"created"
		);

		return dto;
	}
}
