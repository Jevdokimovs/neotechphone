package lv.neotech.service.initialization;

public interface InitializationImporter {
	void execute();
}
