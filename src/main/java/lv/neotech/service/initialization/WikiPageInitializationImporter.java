package lv.neotech.service.initialization;

import lv.neotech.domain.model.CountryDialing;
import lv.neotech.domain.repository.CountryDialingRepository;
import lv.neotech.integration.web.wiki.WikiPageParseException;
import lv.neotech.integration.web.wiki.WikiPhoneCodesPageParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
public class WikiPageInitializationImporter implements InitializationImporter {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private final CountryDialingRepository countryDialingRepository;
	private final WikiPhoneCodesPageParser wikiPhoneCodesPageParser;

	@Autowired
	public WikiPageInitializationImporter(
			CountryDialingRepository countryDialingRepository,
			WikiPhoneCodesPageParser wikiPhoneCodesPageParser
	) {
		this.countryDialingRepository = countryDialingRepository;
		this.wikiPhoneCodesPageParser = wikiPhoneCodesPageParser;
	}

	public void execute() {
		Map<String, Set<String>> countryCallCodesMap;
		try {
			countryCallCodesMap = wikiPhoneCodesPageParser.getParsedDataAsMap();
		} catch (WikiPageParseException e) {
			LOG.error("import data failed, reason: {}", e.getMessage());
			return;
		}

		countryCallCodesMap.forEach((countryCode, callCodes)
				-> saveCountryWithCallCodes(new CountryDialing(countryCode, callCodes))
		);

		long itemsSaved = countryDialingRepository.count();
		LOG.info("total {} items saved", itemsSaved);
	}

	@Transactional
	private void saveCountryWithCallCodes(CountryDialing countryDialing) {
		LOG.info("persist: " + countryDialing.toString());

		countryDialingRepository.save(countryDialing);
	}
}
