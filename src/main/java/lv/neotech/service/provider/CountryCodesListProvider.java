package lv.neotech.service.provider;

import lv.neotech.domain.dto.CountryListItemDTO;
import lv.neotech.domain.dto.CountryListItemDTOConverter;
import lv.neotech.domain.model.CountryDialing;
import lv.neotech.domain.repository.CountryDialingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryCodesListProvider {
	private final CountryDialingRepository countryDialingRepository;
	private final CountryListItemDTOConverter countryListItemDTOConverter;

	@Autowired
	public CountryCodesListProvider(CountryDialingRepository countryDialingRepository, CountryListItemDTOConverter countryListItemDTOConverter) {
		this.countryDialingRepository = countryDialingRepository;
		this.countryListItemDTOConverter = countryListItemDTOConverter;
	}

	public List<CountryListItemDTO> getSortedFullList() {
		List<CountryDialing> entityList = (List<CountryDialing>) countryDialingRepository.findAll();
		List<CountryListItemDTO> dtoList = countryListItemDTOConverter.convertList(entityList);

		return dtoList.stream()
				.sorted(Comparator.comparing(CountryListItemDTO::getCountryCode))
				.collect(Collectors.toList());
	}
}
