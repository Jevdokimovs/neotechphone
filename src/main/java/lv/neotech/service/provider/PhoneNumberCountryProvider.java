package lv.neotech.service.provider;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import lv.neotech.domain.dto.CountryDTO;
import lv.neotech.domain.dto.CountryLookupResultDTO;
import lv.neotech.integration.web.restcountries.RestCountriesNameResolver;
import lv.neotech.service.validation.CountryProvideErrorConstants;
import lv.neotech.service.validation.PhoneNumberValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneNumberCountryProvider {
	private final CallCodePersistenceMatcher callCodePersistenceMatcher;
	private final PhoneNumberValidator phoneNumberValidator;
	private final RestCountriesNameResolver countriesNameResolver;

	@Autowired
	public PhoneNumberCountryProvider(
			CallCodePersistenceMatcher callCodePersistenceMatcher,
			PhoneNumberValidator phoneNumberValidator,
			RestCountriesNameResolver countriesNameResolver
	) {
		this.callCodePersistenceMatcher = callCodePersistenceMatcher;
		this.phoneNumberValidator = phoneNumberValidator;
		this.countriesNameResolver = countriesNameResolver;
	}

	public CountryLookupResultDTO getResult(String phoneNumberStr) {
		CountryLookupResultDTO result = new CountryLookupResultDTO();
		phoneNumberStr = formatPhoneNumberString(phoneNumberStr);

		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber;
		try {
			if (!phoneNumberValidator.isValid(phoneNumberStr)) {
				result.addError(CountryProvideErrorConstants.INVALID_PHONE_FORMAT.toString());
				return result;
			}

			phoneNumber = phoneNumberUtil.parse(phoneNumberStr, "");
		} catch (NumberParseException e) {
			result.addError(CountryProvideErrorConstants.INVALID_PHONE_FORMAT.toString());
			return result;
		}

		callCodePersistenceMatcher.getMatchedCountryCodes(phoneNumber).forEach(countryCode ->
				result.addCountry(new CountryDTO(
						countryCode,
						countriesNameResolver.getByCountryCode(countryCode)
				))
		);

		if (result.getCountries().isEmpty()) {
			result.addError(
					CountryProvideErrorConstants.COUNTRY_NOT_FOUND.toString()
			);
		}

		return result;
	}
	
	private String formatPhoneNumberString(String phoneNumber) {
		phoneNumber = phoneNumber.trim();

		if (!phoneNumber.startsWith("+")) {
			phoneNumber = "+" + phoneNumber;
		}

		return phoneNumber;
	}
}
