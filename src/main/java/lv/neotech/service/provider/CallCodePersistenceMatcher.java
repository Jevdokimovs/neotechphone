package lv.neotech.service.provider;

import com.google.i18n.phonenumbers.Phonenumber;
import lv.neotech.domain.repository.CountryDialingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class CallCodePersistenceMatcher {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private final CountryDialingRepository countryDialingRepository;

	@Autowired
	public CallCodePersistenceMatcher(CountryDialingRepository countryDialingRepository) {
		this.countryDialingRepository = countryDialingRepository;
	}

	Set<String> getMatchedCountryCodes(Phonenumber.PhoneNumber phoneNumber) {
		Set<String> countryCodes = new HashSet<>();
		String countryCode = String.valueOf(phoneNumber.getCountryCode());
		String nationalNumber = String.valueOf(phoneNumber.getNationalNumber());

		String matchCallCode = countryCode;
		countryCodes.addAll(findCountryCodesByCallCodes(matchCallCode));
		LOG.info("try match by: {}", matchCallCode);

		if (countryCodes.isEmpty()) {
			matchCallCode = countryCode
					+ " " + nationalNumber.substring(0, 3);
			LOG.info("try match by: {}", matchCallCode);

			countryCodes.addAll(findCountryCodesStartsWithCode(matchCallCode));
		}

		if (countryCodes.isEmpty()) {
			matchCallCode = countryCode
					+ " " + nationalNumber.substring(0, 1);
			LOG.info("try match by: {}", matchCallCode);

			countryCodes.addAll(findCountryCodesStartsWithCode(matchCallCode));
		}

		return countryCodes;
	}


	@Transactional(readOnly = true)
	private List<String> findCountryCodesByCallCodes(String callCode) {
		return countryDialingRepository.findAllByCallCodesContains(callCode);
	}

	@Transactional(readOnly = true)
	private List<String> findCountryCodesStartsWithCode(String callCode) {
		return countryDialingRepository.findAllByCallCodesStartingWith(callCode);
	}
}
