package lv.neotech.service.validation;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberValidator {
	public boolean isValid(String phoneNumberStr) throws NumberParseException {
		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

		return PhoneNumberUtil.getInstance()
				.isValidNumber(phoneNumberUtil.parse(
						phoneNumberStr, "")
				);
	}
}
