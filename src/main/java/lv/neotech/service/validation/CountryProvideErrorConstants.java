package lv.neotech.service.validation;

public enum CountryProvideErrorConstants {
	INVALID_PHONE_FORMAT,
	COUNTRY_NOT_FOUND
}
