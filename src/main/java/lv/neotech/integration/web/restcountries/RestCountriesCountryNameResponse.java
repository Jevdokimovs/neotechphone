package lv.neotech.integration.web.restcountries;

import com.google.common.base.MoreObjects;

class RestCountriesCountryNameResponse {
	private String name;

	public RestCountriesCountryNameResponse() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("name", name)
				.toString();
	}
}
