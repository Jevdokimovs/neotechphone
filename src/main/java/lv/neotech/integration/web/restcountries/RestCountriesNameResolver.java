package lv.neotech.integration.web.restcountries;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class RestCountriesNameResolver {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private final static int CONNECTION_TIMEOUT_MILLISECONDS = 5000;
	private final static int READ_TIMEOUT_MILLISECONDS = 5000;

	private final RestCountriesNameResolverConfig nameResolverConfig;

	@Autowired
	public RestCountriesNameResolver(RestCountriesNameResolverConfig nameResolverConfig) {
		this.nameResolverConfig = nameResolverConfig;
	}

	public String getByCountryCode(String countryCode) {
		String url = String.format(nameResolverConfig.getPageUrl(), countryCode);
		LOG.info("request URL: " + url);

		RestCountriesCountryNameResponse response;
		try {
			RestTemplate restTemplate = initializeRestTemplate();

			ResponseEntity<RestCountriesCountryNameResponse> responseEntity
					= restTemplate.getForEntity(url, RestCountriesCountryNameResponse.class);
			if (responseEntity.getStatusCode() != HttpStatus.OK) {
				LOG.warn("can't resolve country name for code: {}, leave code as name", countryCode);
				return countryCode;
			}
			response = responseEntity.getBody();
			LOG.info("response: " + response.toString());
		} catch (HttpClientErrorException e) {
			LOG.warn("bad response for url: {}, leave code as name", url);
			return countryCode;
		}

		return response.getName();
	}

	private RestTemplate initializeRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		((SimpleClientHttpRequestFactory)restTemplate
				.getRequestFactory())
				.setConnectTimeout(CONNECTION_TIMEOUT_MILLISECONDS);

		((SimpleClientHttpRequestFactory)restTemplate
				.getRequestFactory())
				.setReadTimeout(READ_TIMEOUT_MILLISECONDS);

		return restTemplate;
	}
}
