package lv.neotech.integration.web.restcountries;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class RestCountriesNameResolverConfig {
	@Value("${neotech.phone-dialing.rest-countries-resolver.name.pageUrl:https://restcountries.eu/rest/v2/alpha/%s?fields=name}")
	private String pageUrl;

	String getPageUrl() {
		return pageUrl;
	}
}
