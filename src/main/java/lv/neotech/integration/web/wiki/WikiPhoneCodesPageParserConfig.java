package lv.neotech.integration.web.wiki;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class WikiPhoneCodesPageParserConfig {
	@Value("${neotech.phone-dialing.wiki-parser.pageUrl:https://en.wikipedia.org/wiki/List_of_country_calling_codes}")
	private String pageUrl;

	@Value("${neotech.phone-dialing.wiki-parser.userAgent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0}")
	private String userAgent;

	@Value("${neotech.phone-dialing.wiki-parser.countryCallCodesRegex:\\+[0-9,\\s]{2,5}+:\\s[A-Z,/,\\s]{2,25}}")
	private String countryCallCodesRegex;

	@Value("${neotech.phone-dialing.wiki-parser.countryAndCodeDelimiter::}")
	private String countryAndCodeDelimiter;

	@Value("${neotech.phone-dialing.wiki-parser.countriesListDelimiter:[/,,/]}")
	private String countriesListDelimiter;

	String getPageUrl() {
		return pageUrl;
	}

	String getUserAgent() {
		return userAgent;
	}

	String getCountryCallCodesRegex() {
		return countryCallCodesRegex;
	}

	String getCountryAndCodeDelimiter() {
		return countryAndCodeDelimiter;
	}

	String getCountriesListDelimiter() {
		return countriesListDelimiter;
	}
}
