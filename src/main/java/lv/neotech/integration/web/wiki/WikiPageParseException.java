package lv.neotech.integration.web.wiki;

public class WikiPageParseException  extends Exception {
	WikiPageParseException(String message, Object... args) {
		super(String.format(message, args));
	}
}

