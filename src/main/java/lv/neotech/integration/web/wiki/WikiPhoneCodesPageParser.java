package lv.neotech.integration.web.wiki;

import com.google.common.collect.Sets;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Strings.nullToEmpty;

@Component
public class WikiPhoneCodesPageParser {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private final WikiPhoneCodesPageParserConfig parserConfig;

	@Autowired
	public WikiPhoneCodesPageParser(WikiPhoneCodesPageParserConfig parserConfig) {
		this.parserConfig = parserConfig;
	}

	/*
	have issue with North America codes(started with +1), not presented in required page
	system will not detect North america codes including USA and Canada
	 */
	public Map<String, Set<String>> getParsedDataAsMap() throws WikiPageParseException {
		List<String> codesMatchedList = new ArrayList<>();
		Element parseBlock = getPageParseBlockElement();

		Matcher matcher = Pattern
				.compile(parserConfig.getCountryCallCodesRegex())
				.matcher(parseBlock.text());

		while (matcher.find()) {
			codesMatchedList.add(matcher.group());
		}

		return mapCountryCodes(codesMatchedList);
	}

	private Element getPageParseBlockElement() throws WikiPageParseException {
		LOG.info("Parse general phone codes from page: " + parserConfig.getPageUrl());

		Document doc;
		try {
			doc = Jsoup.connect(parserConfig.getPageUrl()).userAgent(parserConfig.getUserAgent()).get();
		} catch (IOException e) {
			throw new WikiPageParseException("can't get document for parsing from location: " + parserConfig.getPageUrl());
		}

		return doc.select("table").get(0);
	}

	private Map<String, Set<String>> mapCountryCodes(List<String> codesMatchedList) {
		Map<String, Set<String>> codesMap = new HashMap<>();
		codesMatchedList.forEach(item -> {
			String[] codeAndCountrySplitArray = item.split(parserConfig.getCountryAndCodeDelimiter());

			if (codeAndCountrySplitArray.length != 2) {
				LOG.warn("can't split countries and code for item: {}. Skipped", item);
				return;
			}

			String callCode = nullToEmpty(codeAndCountrySplitArray[0])
					.trim()
					.replaceAll("\\+", "");
			String countries = nullToEmpty(codeAndCountrySplitArray[1])
					.trim()
					.replaceAll("\\s", "");

			if (callCode.isEmpty()) {
				LOG.warn("country code of item {} is empty, item skipped", item);
				return;
			}

			String[] countriesSplitArray = countries.split(parserConfig.getCountriesListDelimiter());
			if (countriesSplitArray.length == 0) {
				LOG.warn("have no items for country code {}, skipped", item);
				return;
			}

			Arrays.asList(countriesSplitArray).forEach(countryCode -> {
				if (!codesMap.containsKey(countryCode)) {
					codesMap.put(countryCode, Sets.newHashSet());
				}
				codesMap.get(countryCode).add(callCode);
			});
		});

		codesMap.entrySet().stream()
				.sorted(Map.Entry.comparingByKey())
				.forEach(entry -> LOG.info(
						"parsed item: {}: {}",
						entry.getKey(), entry.getValue()
						)
				);
		LOG.info("parsed total: {} items", codesMap.size());

		return codesMap;
	}
}
