package lv.neotech.application;

import lv.neotech.service.initialization.InitializationImporter;
import lv.neotech.service.initialization.WikiPageInitializationImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppInitializer {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private final InitializationImporter initializationImporter;

	@Value("${neotech.application-initializer.init-phone-call-codes:false}")
	private boolean initPhoneCallCodes;

	@Autowired
	public AppInitializer(WikiPageInitializationImporter wikiPagePhoneCallCodesImporter) {
		this.initializationImporter = wikiPagePhoneCallCodesImporter;
	}

	public void execute() {
		if (initPhoneCallCodes) {
			LOG.info("Application initializer: load country call codes - start");
			initializationImporter.execute();
			LOG.info("Application initializer: load country call codes - done");
		}
	}
}
