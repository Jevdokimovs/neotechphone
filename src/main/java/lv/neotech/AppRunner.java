package lv.neotech;

import lv.neotech.application.AppInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = {"lv.neotech"})
@EnableAutoConfiguration
public class AppRunner {
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(AppRunner.class, args);

		context.getBean(AppInitializer.class).execute();
	}
}