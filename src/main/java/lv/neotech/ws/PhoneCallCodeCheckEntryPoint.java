package lv.neotech.ws;

import lv.neotech.domain.dto.CountryLookupResultDTO;
import lv.neotech.service.provider.PhoneNumberCountryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("unused")
@RestController
public class PhoneCallCodeCheckEntryPoint {
	private final PhoneNumberCountryProvider phoneNumberCountryProvider;

	@Autowired
	public PhoneCallCodeCheckEntryPoint(PhoneNumberCountryProvider phoneNumberCountryProvider) {
		this.phoneNumberCountryProvider = phoneNumberCountryProvider;
	}

	@RequestMapping(
			value = "ws/check-country-by-phone-number/{phoneNumber}", 
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	public CountryLookupResultDTO checkCountryByPhoneNumber(@PathVariable String phoneNumber) {
		return phoneNumberCountryProvider.getResult(phoneNumber);
	}
}
