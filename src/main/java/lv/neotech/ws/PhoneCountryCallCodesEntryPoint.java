package lv.neotech.ws;

import lv.neotech.domain.dto.CountryListItemDTO;
import lv.neotech.service.provider.CountryCodesListProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SuppressWarnings("unused")
@RestController
public class PhoneCountryCallCodesEntryPoint {
	private final CountryCodesListProvider countryCodesListProvider;

	@Autowired
	public PhoneCountryCallCodesEntryPoint(CountryCodesListProvider countryCodesListProvider) {
		this.countryCodesListProvider = countryCodesListProvider;
	}

	@RequestMapping(
			value = "ws/country-call-codes",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	public List<CountryListItemDTO> fullList() {
		return countryCodesListProvider.getSortedFullList();
	}
}
