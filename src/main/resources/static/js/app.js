var RESULT_MESSAGES = {};
RESULT_MESSAGES['INVALID_PHONE_FORMAT'] = "Phone number has wrong format!";
RESULT_MESSAGES['INVALID_DATA'] = "Entered data is invalid!";
RESULT_MESSAGES['COUNTRY_NOT_FOUND'] = "Can't found country by entered number";

var WS_BASE_URL = "/ws/";

$(document).ready(function () {
	$.views.helpers({
		lowerCase: function (val) {
			return val.toLowerCase();
		},
		getResultMessageText: function (val) {
			if (RESULT_MESSAGES[val] != undefined) {
				return RESULT_MESSAGES[val];
			} else {
				return val;
			}
		},
		formatCallCodesList: function (callCodesList) {
			var index = 1;
			var formattedList = "";
			callCodesList.forEach(function (callCode) {
				formattedList += "+" + callCode;
				if (index < callCodesList.length) {
					formattedList += ", <br>"
				}
				index++;
			});
			return formattedList;
		}
	});

	$("#phoneSubmit").click(function () {
		var phoneNumber = $("#phoneInput").val();
		var resultContainer = $("#lookupResult");

		sendCountryLookupRequest(phoneNumber, resultContainer);
	});

	$("#loadCodesFullListSubmit").click(function () {
		var resultContainer = $("#fullListItemsWrapRow");
		var loadButton = $("#loadCodesFullListSubmit");

		sendFullListLoadRequest(resultContainer, loadButton);
	});
});

function sendFullListLoadRequest(resultContainer, loadButton) {
	loadButton.hide();
	resultContainer.html($("#loadingBar-tmpl").render());
	$.getJSON(
		WS_BASE_URL + "country-call-codes",
		function (data) {
			resultContainer.html($("#countriesFullListRow-tmpl").render(data));
			loadButton.remove();
		}).fail(function (jqxhr) {
			alert('System error, response status: ' + jqxhr.status);
			loadButton.show();
		});
}

function sendCountryLookupRequest(phoneNumber, resultContainer) {
	resultContainer.html($("#loadingBar-tmpl").render());
	$.getJSON(
		WS_BASE_URL + "check-country-by-phone-number/" + phoneNumber,
		function (data) {
			if (data.success) {
				showResult(resultContainer, data.countries);
			} else {
				showErrors(resultContainer, data.errors);
			}
		}).fail(function (jqxhr) {
			showErrors(resultContainer, 'INVALID_DATA');
		});
}

function showResult(resultContainer, countries) {
	resultContainer.html($("#countryInfoRow-tmpl").render(countries));
}

function showErrors(resultContainer, errors) {
	resultContainer.html($("#errorInfoRow-tmpl").render(errors));
}