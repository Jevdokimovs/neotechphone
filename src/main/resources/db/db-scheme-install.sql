CREATE DATABASE IF NOT EXISTS `neotech`
  CHARACTER SET utf8
  COLLATE utf8_general_ci;
USE `neotech`;

-- **Optional CLEANUP
DROP TABLE IF EXISTS `call_code` CASCADE;
DROP TABLE IF EXISTS `country_dialing` CASCADE;