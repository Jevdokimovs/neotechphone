package lv.neotech;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"lv.neotech"})
@EnableAutoConfiguration
public class IntegrationTestTestContext {
}
