package lv.neotech;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = IntegrationTestTestContext.class
)
@TestPropertySource(locations = "classpath:application-test.properties")
@Ignore
public class BaseIntegrationTest {
	@Autowired
	protected TestRestTemplate restTemplate;
}