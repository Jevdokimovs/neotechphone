package lv.neotech.service.provider;

import com.google.common.collect.ImmutableMap;
import lv.neotech.BaseIntegrationTest;
import lv.neotech.domain.dto.CountryDTO;
import lv.neotech.domain.dto.CountryLookupResultDTO;
import lv.neotech.domain.dto.CountryLookupResultDTOBuilder;
import lv.neotech.domain.repository.CountryDialingRepository;
import lv.neotech.service.initialization.WikiPageInitializationImporter;
import lv.neotech.service.validation.CountryProvideErrorConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashSet;

import static org.assertj.core.util.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PhoneNumberCountryProviderTest extends BaseIntegrationTest {
	@Autowired
	private CountryDialingRepository countryDialingRepository;

	@Autowired
	private WikiPageInitializationImporter wikiPageInitializationImporter;

	@Autowired
	private PhoneNumberCountryProvider countryProvider;

	private final ImmutableMap<String, CountryLookupResultDTO> COUNTRY_PROVIDE_RESULTS_EXPECTED
			= ImmutableMap.of(
			"+000 00 00",
			new CountryLookupResultDTOBuilder()
					.success(false)
					.countries(new HashSet<>())
					.errors(newHashSet(Collections.singletonList(
							CountryProvideErrorConstants.INVALID_PHONE_FORMAT.toString())
					))
					.build(),
			"+371 29564343",
			new CountryLookupResultDTOBuilder()
					.success(true)
					.countries(new HashSet<>(
							Collections.singletonList(
									new CountryDTO("LV", "Latvia")
							)))
					.errors(newHashSet())
					.build(),
			"+7 495 1234532",
			new CountryLookupResultDTOBuilder()
					.success(true)
					.countries(new HashSet<>(
							Collections.singletonList(
									new CountryDTO("RU", "Russian Federation")
							)))
					.errors(newHashSet())
					.build(),
			"1 939 2223344",
			new CountryLookupResultDTOBuilder()
					.success(true)
					.countries(new HashSet<>(
							Collections.singletonList(
									new CountryDTO("PR", "Puerto Rico")
							)))
					.errors(newHashSet())
					.build(),
			"+45 32434533",
			new CountryLookupResultDTOBuilder()
					.success(true)
					.countries(new HashSet<>(
							Collections.singletonList(
									new CountryDTO("DK", "Denmark")
							)))
					.errors(newHashSet())
					.build()
	);

	@Before
	public void setUp() {
		wikiPageInitializationImporter.execute();
	}

	@After
	public void tearDown() {
		countryDialingRepository.deleteAll();
	}

	@Test
	public void getResult() {
		COUNTRY_PROVIDE_RESULTS_EXPECTED.forEach((phoneNumber, resultExpected) -> {
			CountryLookupResultDTO result
					= countryProvider.getResult(phoneNumber);
			assertNotNull(result);
			assertEquals(result, resultExpected);
		});
	}
}