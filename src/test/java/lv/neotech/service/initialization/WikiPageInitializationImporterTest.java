package lv.neotech.service.initialization;

import com.google.common.collect.ImmutableMap;
import lv.neotech.BaseIntegrationTest;
import lv.neotech.domain.model.CountryDialing;
import lv.neotech.domain.repository.CountryDialingRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertTrue;

public class WikiPageInitializationImporterTest extends BaseIntegrationTest {
	@Autowired
	private WikiPageInitializationImporter importer;

	@Autowired
	private CountryDialingRepository countryDialingRepository;

	private final ImmutableMap<String, Set<String>> COUNTRIES_WITH_CODES_EXPECTED
			= ImmutableMap.of(
			"LV", newHashSet("371"),
			"KZ", newHashSet("7 6", "7 7"),
			"MP", newHashSet("1 670"),
			"DK", newHashSet("45")
	);

	private final static long PERSISTED_COUNTRIES_MINIMUM_COUNT = 196;

	@Test
	public void execute() {
		importer.execute();

		assertTrue(countryDialingRepository.count() >= PERSISTED_COUNTRIES_MINIMUM_COUNT);
		List<CountryDialing> countriesPersisted = (List<CountryDialing>) countryDialingRepository.findAll();

		COUNTRIES_WITH_CODES_EXPECTED.forEach(
				(countryCodeExpected, callCodesExpected) ->
						assertTrue(
								containsCountryData(
										countriesPersisted,
										countryCodeExpected,
										callCodesExpected
								)
						)
		);
	}

	private boolean containsCountryData(
			List<CountryDialing> testList,
			String countryCodeExpected,
			Set<String> callCodesExpected
	) {
		return testList.stream().anyMatch(item ->
				item.getCountryCode().equals(countryCodeExpected) &&
						item.getCallCodes().equals(callCodesExpected)
		);
	}
}