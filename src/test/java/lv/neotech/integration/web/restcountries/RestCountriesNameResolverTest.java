package lv.neotech.integration.web.restcountries;

import com.google.common.collect.ImmutableMap;
import lv.neotech.BaseIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RestCountriesNameResolverTest extends BaseIntegrationTest{
	@Autowired
	private RestCountriesNameResolver restCountriesNameResolver;

	@Test
	public void getByCountryCode() {
		ImmutableMap<String,String> testCodeValueExpected = ImmutableMap.of(
				"LV", "Latvia",
				"RU", "Russian Federation",
				"BE", "Belgium",
				"AW", "Aruba",
				"UNKNOWN", "UNKNOWN"
		);

		testCodeValueExpected.forEach((code, nameExpected)->{
			String name = restCountriesNameResolver.getByCountryCode(code);
			assertNotNull(name);
			assertEquals(name, nameExpected);
		});
	}
}