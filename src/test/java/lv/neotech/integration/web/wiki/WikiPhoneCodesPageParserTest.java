package lv.neotech.integration.web.wiki;

import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimap;
import lv.neotech.BaseIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class WikiPhoneCodesPageParserTest extends BaseIntegrationTest {
	@Autowired
	private WikiPhoneCodesPageParser pageParser;

	@SuppressWarnings("unchecked")
	@Test
	public void parse() {
		final Multimap<String, Set<String>> countryCodesExpected =
				new ImmutableSetMultimap.Builder<String, Set<String>>()
						.put("LV", newHashSet("371"))
						.put("VI", newHashSet("1 340"))
						.put("XV", newHashSet("882", "883"))
						.put("RU", newHashSet("7 3", "7 4", "7 8", "7 9"))
						.build();

		Map<String, Set<String>> parsedWikiPageDataMap = newHashMap();
		try {
			parsedWikiPageDataMap = pageParser.getParsedDataAsMap();
		} catch (WikiPageParseException e) {
			fail("failed with exception: " + e.getMessage());
		}

		assertNotNull(parsedWikiPageDataMap);
		assertTrue(parsedWikiPageDataMap.size() > 0);

		for (Map.Entry entry : countryCodesExpected.entries()) {
			String code = (String) entry.getKey();
			Set<String> callCodesExpected = (Set<String>) entry.getValue();

			assertTrue(parsedWikiPageDataMap.containsKey(code));
			assertTrue(parsedWikiPageDataMap.get(code).equals(callCodesExpected));
		}
	}
}