package lv.neotech.ws;

import lv.neotech.BaseIntegrationTest;
import lv.neotech.domain.dto.CountryListItemDTO;
import lv.neotech.domain.repository.CountryDialingRepository;
import lv.neotech.service.initialization.WikiPageInitializationImporter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PhoneCountryCallCodesEntryPointTest extends BaseIntegrationTest {
	@Autowired
	private WikiPageInitializationImporter wikiPageInitializationImporter;

	@Autowired
	private CountryDialingRepository countryDialingRepository;

	@Before
	public void setUp() {
		wikiPageInitializationImporter.execute();
	}

	@After
	public void tearDown() {
		countryDialingRepository.deleteAll();
	}

	@Test
	public void fullList() {
		ResponseEntity<CountryListItemDTO[]> responseEntity =
				restTemplate.exchange(
						"/ws/country-call-codes",
						HttpMethod.GET,
						null,
						CountryListItemDTO[].class
				);
		assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);

		List<CountryListItemDTO> countryCallCodesFullList = Arrays.asList(responseEntity.getBody());

		assertNotNull(countryCallCodesFullList);
		assertTrue(countryCallCodesFullList.size() > 0);

		countryDialingRepository.findAll().forEach(countryDialing -> {
			String countryCode = countryDialing.getCountryCode();
			assertTrue(countryCallCodesFullList
					.stream()
					.anyMatch(item-> item.getCountryCode()
							.equals(countryCode))
			);

			CountryListItemDTO dto = countryCallCodesFullList.stream()
					.filter(item -> item.getCountryCode().equals(countryCode))
					.collect(Collectors.toList())
					.get(0);
			assertTrue(dto.getCallCodes().equals(countryDialing.getCallCodes()));
		});
	}
}
