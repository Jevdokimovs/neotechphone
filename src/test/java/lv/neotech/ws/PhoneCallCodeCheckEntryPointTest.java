package lv.neotech.ws;

import lv.neotech.BaseIntegrationTest;
import lv.neotech.domain.dto.CountryDTO;
import lv.neotech.domain.dto.CountryLookupResultDTO;
import lv.neotech.domain.repository.CountryDialingRepository;
import lv.neotech.service.initialization.WikiPageInitializationImporter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;

public class PhoneCallCodeCheckEntryPointTest extends BaseIntegrationTest {
	@Autowired
	private WikiPageInitializationImporter wikiPageInitializationImporter;

	@Autowired
	private CountryDialingRepository countryDialingRepository;

	@Before
	public void setUp() {
		wikiPageInitializationImporter.execute();
	}

	@After
	public void tearDown() {
		countryDialingRepository.deleteAll();
	}

	@Test
	public void checkCountryByPhoneNumber() {
		String testPhoneNumber = "+37129434343";
		ResponseEntity<CountryLookupResultDTO> responseEntity =
				restTemplate.exchange(
						"/ws/check-country-by-phone-number/" + testPhoneNumber,
						HttpMethod.GET,
						null,
						CountryLookupResultDTO.class
				);
		assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
		assertNotNull(responseEntity.getBody());
		assertThat(responseEntity.getBody(), instanceOf(CountryLookupResultDTO.class));

		CountryLookupResultDTO countryLookupResult = responseEntity.getBody();
		assertTrue(countryLookupResult.isSuccess());
		assertTrue(countryLookupResult.getErrors().isEmpty());
		assertTrue(countryLookupResult.getCountries().size() == 1);

		CountryDTO country = new ArrayList<>(countryLookupResult.getCountries()).get(0);
		assertNotNull(country);
		assertEquals(country.getCode(), "LV");
		assertEquals(country.getName(), "Latvia");
	}
}
